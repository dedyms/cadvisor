# Builder
FROM golang:latest as builder
ARG VERSION=$VERSION
ENV CADVISOR_VERSION=$VERSION
RUN apt-get update && apt-get install -y git dmsetup && apt-get clean
RUN git clone --branch release-${CADVISOR_VERSION} https://github.com/google/cadvisor.git /go/src/github.com/google/cadvisor
WORKDIR /go/src/github.com/google/cadvisor
RUN make build

# Image for usage
FROM debian:bullseye-slim
COPY --from=builder /go/src/github.com/google/cadvisor/cadvisor /usr/bin/cadvisor
ENTRYPOINT ["/usr/bin/cadvisor", "-logtostderr", "-docker_only"]